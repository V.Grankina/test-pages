const burgerBtn = document.querySelector(".burger-btn");
const burgerDrop = document.querySelector('.header-nav__list')

burgerBtn.onclick = function (event){
    burgerBtn.classList.toggle('burger-btn--open');
    burgerDrop.classList.toggle('header-nav__list--open');
}